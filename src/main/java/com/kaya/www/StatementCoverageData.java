package com.kaya.www;

import java.io.FileWriter;
import java.util.HashSet;
import java.util.Iterator;

public class StatementCoverageData {
    static HashSet<String> hits = new HashSet<String>();


    public static void lineExecuted(String str) {
        hits.add(str);
    }


    public static void writeIntoFile(FileWriter writer) {
        try {
            Iterator iterator = hits.iterator();
            StringBuilder sb = new StringBuilder();
            sb.append("[");

            hits.stream().sorted().forEach(e -> {
                sb.append(e + ",");
            });


            writer.write(  sb.substring(0,sb.length()-1).concat("]").toString());


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}


