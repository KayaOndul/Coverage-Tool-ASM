package com.kaya.www;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;

import java.io.File;
import java.io.FileWriter;

public class CommonListener extends RunListener
{
    static FileWriter writer;

    public void testRunStarted(Description description) throws java.lang.Exception
    {
        try
        {
            File file = new File("stmt-cov.txt");
            if (file.exists())
                file.delete();
            else
                file.createNewFile();
            writer = new FileWriter("stmt-cov.txt",false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

        }
    }

    public void testRunFinished(Result result) throws java.lang.Exception
    {
        StatementCoverageData.writeIntoFile(writer);
        writer.close();
    }

    public void testFinished(Description description) throws java.lang.Exception
    {

    }



    public void testStarted(Description description) throws java.lang.Exception
    {

    }
}
