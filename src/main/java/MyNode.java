import java.util.HashSet;
import java.util.Set;

public class MyNode {
    String line ;
    Integer hits ;

    public MyNode(String line, Integer hits) {
        this.line = line;
        this.hits = hits;
    }
}
