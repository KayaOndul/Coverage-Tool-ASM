import com.kaya.www.CommonListener;
import org.junit.runner.JUnitCore;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class MyTestRunner {
    public static String[] arr = new String[]{
        "Outputs/add.txt",
        "Outputs/deneme.txt",
        "Outputs/div.txt",
        "Outputs/mult.txt",
        "Outputs/multiIfElse.txt",
        "Outputs/other.txt",
        "Outputs/sub.txt",
        "Outputs/uncovered.txt",
    };

    public static void main(String[] args) throws IOException {
        Map<String, FinalInfo> finalInfo = FirstVisitor.main(new String[]{"target/classes/CalculatorInstrumented.class", " target/classes/CalculatorInstrumented.class.bak"});
        JUnitCore runner = new JUnitCore();
        runner.addListener(new CommonListener());
        runner.run(TestSuite.class);
        HashSet<String> hits = getHits();


        HashMap<String, HashMap<String, Set<String>>> map = new HashMap<>();
        for (String s : arr) {
            File f = new File(s);
            FileInputStream stream = new FileInputStream(f);
            InputStreamReader conexion = new InputStreamReader(stream);
            LineNumberReader reader = new LineNumberReader(conexion);

            String x;

            while ((x = reader.readLine()) != null) {
                if (x.length() < 0 || x.equals("")) {
                    break;
                }
                if (x.startsWith("digraph")) {
                    continue;
                }

                String[] a = x.split(" -> ");
                a = Arrays.stream(a).map(e -> e.trim()).toArray(String[]::new);
                if (map.containsKey(s)) {
                    HashMap<String, Set<String>> temp = map.get(s);
                    if (temp.containsKey(a[0])) {
                        Set<String> tempSet = temp.get(a[0]);
                        tempSet.add(a[1]);
                        temp.put(a[0], tempSet);
                        map.put(s, temp);
                    } else {
                        Set<String> tempSet = new HashSet<>();
                        tempSet.add(a[1]);
                        temp.put(a[0], tempSet);
                        map.put(s, temp);
                    }
                } else {
                    HashMap temp = new HashMap<String, Set<String>>();
                    Set<String> tempSet = new HashSet<>();
                    tempSet.add(a[1]);
                    temp.put(a[0], tempSet);
                    map.put(s, temp);
                }
            }
        }

        HashMap<String, Set<String>> importantNodes = new HashMap<>();

        for (Map.Entry e : map.entrySet()) {
            String methodName = (String) e.getKey();
            HashMap<String, Set<String>> map1 = (HashMap<String, Set<String>>) e.getValue();

            Set<String> lines = new HashSet<>();
            for (Map.Entry entry : map1.entrySet()) {
                String line = (String) entry.getKey();
                Set<String> counts = (Set<String>) entry.getValue();
                if (counts.size() > 1) {
                    lines.add(line);
                }
            }
            importantNodes.put(methodName.replace("Outputs/", "").replace(".txt", ""), lines);


        }


        String fileName = "coverage.txt";

        File fout = new File(fileName);
        if (fout.exists())
            fout.delete();
        else
            fout.createNewFile();


        FileWriter fw = new FileWriter("coverage.txt", false);

        printResult(fw, finalInfo, hits, importantNodes);
        fw.close();


    }

    private static HashSet<String> getHits() throws IOException {
        HashSet<String> hits = new HashSet<>();


        File f = new File("stmt-cov.txt");
        FileInputStream stream = new FileInputStream(f);
        InputStreamReader conexion = new InputStreamReader(stream);
        LineNumberReader reader = new LineNumberReader(conexion);

        String x;


        while ((x = reader.readLine()) != null) {
            Arrays.asList(x.split(",")).stream().sorted().forEach(e -> {
                hits.add(e.replace("]", "").replace("[", ""));
            });

        }
        return hits;
    }

    public static void printResult(FileWriter writer, Map<String, FinalInfo> finalInfo, HashSet hits, HashMap<String, Set<String>> importantNodes) throws IOException {
        for (Map.Entry<String, FinalInfo> entry : finalInfo.entrySet()) {
            writer.write("class: " + entry.getKey() + "\n");


            AtomicReference<Integer> sumLines = new AtomicReference<>(0);
            Integer totalBranchesAll=0;
            Integer coveredBranches=0;
            Integer sumMethods = 0;
            Integer noCall = 0;
            entry.getValue().first.map.entrySet().forEach(e -> {
                sumLines.updateAndGet(v -> v + e.getValue().size());
            });
            for (Map.Entry<FirstPassInfo.MethodsInfo, SortedSet<Integer>> entry1 : entry.getValue().first.map.entrySet()) {
                String methodName = entry1.getKey().getMethodName();
                sumMethods++;
                writer.write("method: " + entry1.getKey() + "\n");

                writer.write("all statements: " + entry1.getValue() + "\n");

                Set<Integer> set = entry1.getValue();
                writer.write("covered statements: ");
                writer.write("[");
                StringBuilder stringBuilder = new StringBuilder();
                set.forEach(e -> {
                    if (hits.contains(e.toString())) {
                        stringBuilder.append(e + ", ");
                    }
                });
                noCall++;
                if (stringBuilder.length() > 2) {

                    writer.write(stringBuilder.substring(0, stringBuilder.length() - 2) + "]\n");
                } else {
                    noCall--;
                    writer.write("]\n");
                }

                long counterSt;
                Set<Integer> allLines = entry1.getValue();

                counterSt = hits.stream().filter(e -> {
                    if(((String)e).equals("")){
                        return false;
                    }
                 return   allLines.contains(Integer.parseInt((String) e));
                }).count();

                //STATEMENT COVERAGE CALC
                Double d = Double.valueOf(counterSt) / Double.valueOf(entry1.getValue().size()) * 100;
                String stmtCov = String.format("%.02f", d);
                writer.write("statement coverage: " + stmtCov + "%\n");



                //BRANCH COVERAGE CALC
                writer.write("branch coverage: ");
                AtomicInteger counter = new AtomicInteger();
                Integer totalBranch = 0;
                if (!importantNodes.containsKey(methodName) || importantNodes.get(methodName).size() < 1) {
                    writer.write("N/A\n");
                } else {
                    totalBranch = importantNodes.get(methodName).size() * 2;
                    totalBranchesAll+=totalBranch;
                    importantNodes.get(methodName).forEach(e -> {
                        if (hits.contains(e)) {
                            counter.getAndIncrement();
                        }
                        ;

                    });
                    coveredBranches+=counter.intValue();
                    Double result = counter.doubleValue() / Double.valueOf(totalBranch) * 100;
                    writer.write(String.format("%.02f", result) + "%\n");
                }


                writer.write("--------------------------------------------" + "\n");
            }

            writer.write("ALL SUITE RESULTS" + "\n");

            String brCov=String.format("%.02f%s (Covered/Total): %d / %d", Double.valueOf(coveredBranches)/Double.valueOf(totalBranchesAll)*100,"%",coveredBranches,totalBranchesAll) ;
            writer.write("branch coverage: "+brCov + "\n");
            Double d = (hits.size() / (double) sumLines.get()) * 100;
            String stmtCov = String.format("%.02f%s (Hit/Total): %d / %d", d,"%",hits.size(),sumLines.get());
            writer.write("statement coverage: " + stmtCov + "\n");

            String mtCov = String.format("%.02f%s (Methods/Total): %d / %d", ((double) (noCall - 1) / (double) (sumMethods - 1))*100,"%",noCall - 1,sumMethods - 1);
            writer.write("method coverage: " +mtCov+"\n");
            writer.write("\n");
        }
    }
}
