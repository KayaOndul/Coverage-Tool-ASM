import org.objectweb.asm.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;


public class Instrumenter {

    static class ClassPrinter extends ClassVisitor {

        private String cName;

        class MethodPrinter extends MethodVisitor implements Opcodes {

            public MethodPrinter(MethodVisitor mv) {
                super(ASM5, mv);
            }



            @Override
            public void visitLineNumber(int line, Label start) {


                mv.visitLdcInsn( Integer.toString(line));
                mv.visitMethodInsn(INVOKESTATIC, "com/kaya/www/StatementCoverageData", "lineExecuted", "(Ljava/lang/String;)V", false);
                super.visitLineNumber(line, start);
            }


        }

        public ClassPrinter(final ClassVisitor cv) {
            super(Opcodes.ASM5, cv);
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            cName = name;
            super.visit(version, access, name, signature, superName, interfaces);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
            MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
            return mv == null ? null : new MethodPrinter(mv);
        }

    }
    public static void main(final String args[]) throws Exception {
        FileInputStream is = new FileInputStream(args[0]);
        byte[] b;

        ClassReader cr = new ClassReader(is);
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        ClassVisitor cv = new ClassPrinter(cw);
        cr.accept(cv, 0);
        b = cw.toByteArray();


        FileOutputStream fos = new FileOutputStream(args[1]);
        fos.write(b);
        fos.close();

    }

}




