import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;

public class FirstVisitor {
    private static final Map<String, FinalInfo> finalInfo = new HashMap<>();

    public FirstVisitor(String projectName) {
        this.projectName = projectName;
    }



    private final String projectName;

    public static Map<String, FinalInfo> main(String[] args) throws IOException {
        FileInputStream is = new FileInputStream(args[0]);

        byte[] b;
        FirstPassInfo info = new FirstPassInfo();
        ClassReader cr = new ClassReader(is);
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        InformationCollector ca = new InformationCollector(cw, info);
        cr.accept(ca, 0);


        String className="CalculatorInstrumented";
        finalInfo.put(className, new FinalInfo(info, new SecondPassInfo()));
        return finalInfo;




    }







    private byte[] passOne(byte[] classByte, FirstPassInfo info, String name) {
        byte[] ret;
        ClassReader cr = new ClassReader(classByte);
        ClassWriter cw = new ClassWriter(cr, 0);
       InformationCollector ca = new InformationCollector(cw, info);
        cr.accept(ca, 0);
        ret = cw.toByteArray();
        return ret;
    }


}
