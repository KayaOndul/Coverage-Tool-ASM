## HW3 SWE 550


To find out the all linenumbers in CalculatorInstrumented class. I wrote FirstVisitor adapter to get all lines. (Modification 
of the code resides in https://github.com/SabaPing/java-test-coverage-tool-asm/tree/master/src )
After getting all the lines,
I used ASMifier to find out which method calls are converted to ASM instructions with this, mostly try and error.

```
java -cp .:asm-all-5.0.3.jar jdk.internal.org.objectweb.asm.util.ASMifier target/classes/CalculatorInstrumented.class

```

After finding out how to make calls StatementCoverageData's lineExecuted method,I put a listener to junit to fetch test start and end of test. Lines executed recorded on a HashMap and printed out to
stmt-cov.txt in base directory.   After recording lines hit, all lines and generated control flow graph,
 i calculated statement coverage,method coverage.For the branch coverage i used cfg, if a node has more than 1 edge it is a jump node
 , therefore I saved them in a seperate map. If a method doesn't have a jumpnode branch coverage is N/A.
 In printout i multiplied number of jumpNodes with two to find out total branches.To find out covered branches,
  and check whether jumpnode lines are hit. Then i divided hit jumpnodes to total branches to find out coverage


## TO RUN IT 

 ````
-run mvn clean install
-run java MyTestrunner

```
